#include<stdio.h>
#include<string.h>
struct employee{
    int id;
    char name[100];
    char address[100];
    float salary;
};
typedef struct employee emp;
int input()
{
    int n;
    printf("enter the number of employees\n");
    scanf("%d",&n);
    return n;
}
void input_data(int n, emp s[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the info of employee%d:\n", i+1);
        printf("enter ID_no:\n");
        scanf("%d",&s[i].id);
        printf("enter employee name:\n");
        gets(s[i].name);
        printf("enter postal address of employee:\n");
        gets(s[i].address);
        printf("enter salary:\n");
        scanf("%f",&s[i].salary);
    }
}
void display(int n, emp s[n])
{
    for(int i=0;i<n;i++)
    {
        printf("details of employee%d:\n", i+1);
        printf("ID-no:%d\n",s[i].id);
        printf("name of employee:%s\n", s[i].name);
        printf("poatal address of employee:%s\n",s[i].address);
        printf("salary:%f\n",s[i].salary);
    }
}
int main()
{
    int n;
    n=input();
    emp s[n];
    input_data(n,s);
    display(n,s);
    return 0;
}
