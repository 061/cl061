#include<stdio.h>
int by_val(int, int);
int by_ref(int *, int *);
int main()
{
	int a,b;
	printf("enter two numbers to be SWAPED\n");
	scanf("%d%d",&a,&b);
	printf("the entered numbers are: A=%d and B=%d \n",a,b);
	by_val(a,b);
	printf("swaping done by using *CALL BY VALUE* method: A=%d and B=%d\n",a,b);
	by_ref(&a, &b);
	printf("swaping done by using *CALL BY REFERENCE* method: A=%d and B=%d\n",a,b);
	return(0);
}
int by_val(int p, int q)
{
	int x;
	x=p;
	p=q;
	q=x;
	return(p);
	return(q);
	
}
int by_ref(int *x, int *y)
{
	int z;
	z=*x;
	*x=*y;
	*y=z;
	return(*x);
	return(*y);
}


