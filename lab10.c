#include<stdio.h>
#include<math.h>
int main()
{
	double a,b,c,eq,root_1,root_2,real_part,img_part;
	printf("enter coeffiients of a,b,c:\n");
	scanf("%lf%lf%lf",&a,&b,&c);
	eq=(b*b)-(4*a*c);
	if(eq>0)
	{
		root_1=(-b+sqrt(eq))/(2*a);
		root_2=(-b-sqrt(eq))/(2*a);

		printf("root1=%.21f and root2=%.21f\n",root_1,root_2);
	}
	else if(eq==0)
	{
		root_1=root_2=-b/(2*a);
		printf("root1=root2=%.21f;]\n",root_1);
	}
	else
	{
		real_part=-b/(2*a);
		img_part=sqrt(-eq)/(2*a);
		printf("root1=%.21f+%.21fi and root2=%.2f-%.2fi\n",real_part,img_part,real_part,img_part);
	}
	return(0);
}
