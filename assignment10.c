#include<stdio.h>
int gcd (int, int);
int main()
{
	int n1, n2;
	printf("enter two integer elements:\n");
	scanf("%d%d",&n1,&n2);
	printf("GREATEST COMMON DIVISOR of %d and %d is: %d",n1,n2,gcd(n1, n2));
	return(0);
}
int gcd(int a,int b)
{
	int i, gcd;
	for(i=1;i<=a&&i<=b;++i)
	{
		if(a%i==0&&b%i==0)
			gcd=i;
	}
	return gcd;
}
