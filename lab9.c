#include<stdio.h>
int main()
{
	int a,b,*p,*q;
	p=&a;
	q=&b;
	printf("enter two integers to perform ARITHMETIC OPERATIONS\n");
	scanf("%d%d",&a,&b);
	printf("THE ARITHMETIC OPERATION OF %d AND %d ARE:\n",*p,*q);
	printf("SUM: %d + %d = %d\n",*p,*q,(*p+*q));
	printf("DIFFERENCE: %d - %d = %d\n",*p,*q,(*p-*q));
	printf("PRODUCT: %d x %d = %d\n",*p,*q,((*p)*(*q)));
	printf("QUOTIENT: %d / %d = %d\n",*p,*q,((*p)/(*q)));
	printf("REMINDER:%d / %d = %d\n",*p,*q,(*p%*q));
	return(0);
}
